/* Dooba SDK
 * SD Card Driver
 */

#ifndef	__SDCARD_BLKCACHE_H
#define	__SDCARD_BLKCACHE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "sdcard.h"

// Cache Size (Entries)
#ifndef	SDCARD_BLKCACHE_SIZE
#define	SDCARD_BLKCACHE_SIZE					2
#endif

// Entry Structure
struct sdcard_blkcache_entry
{
	// Dirty?
	uint8_t dirty;

	// Block Index
	uint32_t blk;

	// Data
	uint8_t data[SDCARD_BLOCK_SIZE];
};

// Block Cache Structure
struct sdcard_blkcache
{
	// Entries
	struct sdcard_blkcache_entry entries[SDCARD_BLKCACHE_SIZE];

	// Order (first is most recently used)
	uint8_t order[SDCARD_BLKCACHE_SIZE];
};

// Initialize Block Cache
extern void sdcard_blkcache_init(struct sdcard_blkcache *bc);

// Synchronize Block Cache
extern uint8_t sdcard_blkcache_sync(struct sdcard_blkcache *bc, struct sdcard *sdc);

// Get Block
extern struct sdcard_blkcache_entry *sdcard_blkcache_get(struct sdcard_blkcache *bc, struct sdcard *sdc, uint32_t blk);

#endif
