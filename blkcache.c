/* Dooba SDK
 * SD Card Driver
 */

// External Includes
#include <string.h>

// Internal Includes
#include "blkcache.h"

// Initialize Block Cache
void sdcard_blkcache_init(struct sdcard_blkcache *bc)
{
	uint8_t i;

	// Reset Entries
	for(i = 0; i < SDCARD_BLKCACHE_SIZE; i = i + 1)									{ bc->order[i] = 0xff; bc->entries[i].blk = 0xffffffff; bc->entries[i].dirty = 0; }
}

// Synchronize Block Cache
uint8_t sdcard_blkcache_sync(struct sdcard_blkcache *bc, struct sdcard *sdc)
{
	uint8_t i;

	// Synchronize Entries
	for(i = 0; i < SDCARD_BLKCACHE_SIZE; i = i + 1)
	{
		if((bc->entries[i].dirty) && (bc->entries[i].blk != 0xffffffff))			{ if(sdcard_write_with_retry(sdc, bc->entries[i].blk, bc->entries[i].data, 1)) { return 1; } else { bc->entries[i].dirty = 0; } }
	}

	return 0;
}

// Get Block
struct sdcard_blkcache_entry *sdcard_blkcache_get(struct sdcard_blkcache *bc, struct sdcard *sdc, uint32_t blk)
{
	struct sdcard_blkcache_entry *e;
	uint8_t i;
	uint8_t done;
	uint8_t p;

	// Run through Entries
	i = 0;
	done = 0;
	while((!done) && (i < SDCARD_BLKCACHE_SIZE))
	{
		// Check End
		if(bc->order[i] == 0xff)													{ done = 1; }
		else
		{
			// Check Entry
			if(bc->entries[bc->order[i]].blk == blk)								{ done = 1; }
			else																	{ i = i + 1; }
		}
	}

	// Check Cache Full
	if(i == SDCARD_BLKCACHE_SIZE)
	{
		// Commit oldest block to free up a slot
		p = SDCARD_BLKCACHE_SIZE - 1;
		e = &(bc->entries[bc->order[p]]);
		if(e->dirty)																{ if(sdcard_write_with_retry(sdc, e->blk, e->data, 1)) { return 0; } else { e->dirty = 0; } }
		e->blk = 0xffffffff;
	}
	else
	{
		// Check End
		if(bc->order[i] == 0xff)
		{
			// Cache is not full - just use this (first free) slot
			p = i;
			i = 0;
			done = 0;
			while((!done) && (i < SDCARD_BLKCACHE_SIZE))							{ if(bc->entries[i].blk == 0xffffffff) { done = 1; } else { i = i + 1; } }
			if(!done)																{ return 0; }
			bc->order[p] = i;
		}
		else
		{
			// Did we actually find what we wanted?
			if(bc->entries[bc->order[i]].blk != blk)								{ return 0; }
			p = i;
		}
	}

	// Acquire Cache Entry
	e = &(bc->entries[bc->order[p]]);

	// Do we need to load the block?
	if(e->blk != blk)
	{
		// Load the block & Mark it
		if(sdcard_read_with_retry(sdc, blk, e->data, 1))							{ return 0; }
		e->blk = blk;
	}

	// Reorder Cache
	done = bc->order[p];
	for(i = p; i > 0; i = i - 1)													{ bc->order[i] = bc->order[i - 1]; }
	bc->order[0] = done;

	return e;
}
