/* Dooba SDK
 * SD Card Driver
 */

#ifndef	__SDCARD_H
#define	__SDCARD_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Command Prefix
#define	SDCARD_CMD_PREFIX								0x40

// Commands
#define	SDCARD_CMD_IDLE									0x00
#define	SDCARD_CMD_INIT									0x01
#define	SDCARD_CMD_CHKV									0x08
#define	SDCARD_CMD_RCSD									0x09
#define	SDCARD_CMD_STOP									0x0c
#define	SDCARD_CMD_STAT									0x0d
#define	SDCARD_CMD_SBLK									0x10
#define	SDCARD_CMD_RBLK									0x11
#define	SDCARD_CMD_RMUL									0x12
#define	SDCARD_CMD_WBLK									0x18
#define	SDCARD_CMD_WMUL									0x19
#define	SDCARD_CMD_ACMD									0x37
#define	SDCARD_CMD_ROCR									0x3a

// App Commands (Must send CMD_ACMD first)
#define	SDCARD_ACMD_INIT								0x29

// CHKV Standard Value
#define	SDCARD_CHKV_DEF_VAL								0x000001aa

// Init Command Flags
#define	SDCARD_ACMD_INIT_FLAG_HCS						0x40000000

// OCR Flags
#define	SDCARD_OCR_FLAG_CCS								0x40000000

// CSD
#define	SDCARD_CSD_LEN									32

// State Flags
#define	SDCARD_FLAG_IDLE								0x01
#define	SDCARD_FLAG_ERASE_RST							0x02
#define	SDCARD_FLAG_ILLEGAL_CMD							0x04
#define	SDCARD_FLAG_CRC_ERROR							0x08
#define	SDCARD_FLAG_ERASE_ERROR							0x10
#define	SDCARD_FLAG_ADDR_ERROR							0x20
#define	SDCARD_FLAG_PARAM_ERROR							0x40

// Data Packet Tokens
#define	SDCARD_DATA_TOKEN_WRITE							0xfc
#define	SDCARD_DATA_TOKEN_STOP							0xfd
#define	SDCARD_DATA_TOKEN_READ							0xfe

// Data Response Tokens
#define	SDCARD_DATA_RESP_MASK							0x1f
#define	SDCARD_DATA_RESP_OK								0x05
#define	SDCARD_DATA_RESP_CRC_ERROR						0x0b
#define	SDCARD_DATA_RESP_WRITE_ERROR					0x0d

// I/O Chunk Size
#define	SDCARD_IO_CHUNK_SIZE							64

// I/O Directions
#define	SDCARD_READ										0
#define	SDCARD_WRITE									1

// Timeouts
#define	SDCARD_CMD_TIMEOUT								50000
#define	SDCARD_INIT_TIMEOUT								300
#define	SDCARD_IO_TIMEOUT								20000

// Read / Write Retry Delay (ms)
#define	SDCARD_RETRY_DELAY								1000

// Max Read / Write Retry Count
#define	SDCARD_MAX_RETRY								10

// Init Clock Pulses
#define	SDCARD_INIT_SCK_PULSES							100

// Delays
#define	SDCARD_DELAY_INIT								100
#define	SDCARD_DELAY_SELECT								20

// Block Size (yes, 512 bytes)
#define	SDCARD_BLOCK_SIZE								512

// Card Types
#define	SDCARD_TYPE_SD									0
#define	SDCARD_TYPE_SDHC								1

// Block for Address
#define	sdcard_blk(addr)								(addr / SDCARD_BLOCK_SIZE)
#define	sdcard_blk_off(addr)							(addr % SDCARD_BLOCK_SIZE)

// SD Card Structure
struct sdcard
{
	// CS Pin
	uint8_t cs_pin;

	// Card Type
	uint8_t sdcard_type;

	// Capacity (Sectors)
	uint32_t sdcard_capacity;
};

// Initialize SD Card
extern void sdcard_init(struct sdcard *sdc, uint8_t cs_pin);

// Pre-Initialize SD Card
extern void sdcard_preinit(uint8_t cs_pin);

// Mount Card (sets sdcard_type and sdcard_capacity)
extern uint8_t sdcard_init_card(struct sdcard *sdc);

// Init CHKV
extern uint8_t sdcard_init_chkv(struct sdcard *sdc);

// Read Blocks
extern uint8_t sdcard_read_blocks(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count);

// Write Blocks
extern uint8_t sdcard_write_blocks(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count);

// Read Blocks with retry
extern uint8_t sdcard_read_with_retry(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count);

// Write Blocks with retry
extern uint8_t sdcard_write_with_retry(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count);

// Issue App Command (selects card)
extern uint8_t sdcard_issue_acmd(struct sdcard *sdc, uint8_t cmd, uint32_t arg);

// Issue Simple Command (selects card)
extern uint8_t sdcard_issue_cmd(struct sdcard *sdc, uint8_t cmd, uint32_t arg);

// Read OCR
extern uint32_t sdcard_read_ocr(struct sdcard *sdc);

// Read CSD
extern uint8_t sdcard_read_csd(struct sdcard *sdc, uint8_t *buffer);

// Send Command - Does not select Card!
extern void sdcard_cmd(struct sdcard *sdc, uint8_t cmd, uint32_t arg);

// Wait until equal
extern uint8_t sdcard_wait_until_equal(struct sdcard *sdc, uint8_t x, uint16_t timeout);

// Wait until different
extern uint8_t sdcard_wait_until_diff(struct sdcard *sdc, uint8_t x, uint16_t timeout);

#endif
