/* Dooba SDK
 * SD Card Driver
 */

// External Includes
#include <string.h>

// Internal Includes
#include "storage.h"

// Initialize SD Card as Storage Device
void sdcard_storage_init(struct sdcard_storage *sdcs, uint8_t cs_pin)
{
	// Initialize SD Card Driver
	sdcard_init(&(sdcs->sdc), cs_pin);

	// Attempt to Initialize SD Card
	if(sdcard_init_card(&(sdcs->sdc)))																{ sdcs->state = SDCARD_STORAGE_OFF; }
	else																							{ sdcs->state = SDCARD_STORAGE_RDY; }

	// Initialize Block Cache
	sdcard_blkcache_init(&(sdcs->blkcache));

	// Initialize & Register Storage
	storage_init_dev(&(sdcs->st), sdcs, (uint64_t)(sdcs->sdc.sdcard_capacity) * SDCARD_BLOCK_SIZE, (storage_io_t)sdcard_storage_read, (storage_io_t)sdcard_storage_write, (storage_sync_t)sdcard_storage_sync, "sdc.%i", cs_pin);
	storage_register(&(sdcs->st));
}

// Read Data
uint8_t sdcard_storage_read(struct storage *s, struct sdcard_storage *sdcs, uint64_t addr, void *data, uint16_t size)
{
	struct sdcard_blkcache_entry *e;
	uint32_t blk;
	uint32_t off;
	uint16_t ss;
	uint8_t *p;

	// Re-Init if necessary
	if(sdcs->state == SDCARD_STORAGE_OFF)
	{
		// Attempt Re-Init
		if(sdcard_init_card(&(sdcs->sdc)))															{ return 1; }
		else
		{
			// Re-Init Success
			sdcs->state = SDCARD_STORAGE_RDY;
			s->size = (uint64_t)(sdcs->sdc.sdcard_capacity) * SDCARD_BLOCK_SIZE;
		}
	}

	// Loop
	p = data;
	while(size)
	{
		// Get Block index & offset
		blk = sdcard_blk(addr);
		off = sdcard_blk_off(addr);

		// Determine how much we can write for this block
		ss = size;
		if((off + ss) > SDCARD_BLOCK_SIZE)															{ ss = SDCARD_BLOCK_SIZE - off; }

		// Acquire Block from Cache
		e = sdcard_blkcache_get(&(sdcs->blkcache), &(sdcs->sdc), blk);
		if(!e)																						{ sdcs->state = SDCARD_STORAGE_OFF; return 1; }

		// Perform I/O
		memcpy(p, &(e->data[off]), ss);

		// Next
		p = &(p[ss]);
		addr = addr + ss;
		size = size - ss;
	}

	return 0;
}

// Write Data
uint8_t sdcard_storage_write(struct storage *s, struct sdcard_storage *sdcs, uint64_t addr, void *data, uint16_t size)
{
	struct sdcard_blkcache_entry *e;
	uint32_t blk;
	uint32_t off;
	uint16_t ss;
	uint8_t *p;

	// Re-Init if necessary
	if(sdcs->state == SDCARD_STORAGE_OFF)
	{
		// Attempt Re-Init
		if(sdcard_init_card(&(sdcs->sdc)))															{ return 1; }
		else
		{
			// Re-Init Success
			sdcs->state = SDCARD_STORAGE_RDY;
			s->size = (uint64_t)(sdcs->sdc.sdcard_capacity) * SDCARD_BLOCK_SIZE;
		}
	}

	// Loop
	p = data;
	while(size)
	{
		// Get Block index & offset
		blk = sdcard_blk(addr);
		off = sdcard_blk_off(addr);

		// Determine how much we can write for this block
		ss = size;
		if((off + ss) > SDCARD_BLOCK_SIZE)															{ ss = SDCARD_BLOCK_SIZE - off; }

		// Acquire Block from Cache
		e = sdcard_blkcache_get(&(sdcs->blkcache), &(sdcs->sdc), blk);
		if(!e)																						{ sdcs->state = SDCARD_STORAGE_OFF; return 1; }

		// Perform I/O & Mark Dirty
		memcpy(&(e->data[off]), p, ss);
		e->dirty = 1;

		// Next
		p = &(p[ss]);
		addr = addr + ss;
		size = size - ss;
	}

	return 0;
}

// Synchronize
uint8_t sdcard_storage_sync(struct storage *s, struct sdcard_storage *sdcs)
{
	// Synchronize Block Cache
	return sdcard_blkcache_sync(&(sdcs->blkcache), &(sdcs->sdc));
}
