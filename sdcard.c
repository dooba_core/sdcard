/* Dooba SDK
 * SD Card Driver
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <spi/spi.h>
#include <dio/dio.h>
#include <util/delay.h>
#include <util/bswap.h>

// Internal Includes
#include "sdcard.h"

// Initialize SD Card
void sdcard_init(struct sdcard *sdc, uint8_t cs_pin)
{
	// Pre-Init
	sdcard_preinit(cs_pin);

	// Set CS Pin
	sdc->cs_pin = cs_pin;

	// Set Type (Default: SD)
	sdc->sdcard_type = SDCARD_TYPE_SD;

	// Clear Capacity
	sdc->sdcard_capacity = 0;
}

// Pre-Initialize SD Card
void sdcard_preinit(uint8_t cs_pin)
{
	// Pre-Init
	dio_output(cs_pin);
	dio_hi(cs_pin);
}

// Init Card
uint8_t sdcard_init_card(struct sdcard *sdc)
{
	uint8_t csd[SDCARD_CSD_LEN];
	uint16_t i;

	// Clear Capacity
	sdc->sdcard_capacity = 0;

	// Pulse Clock Line
	dio_hi(sdc->cs_pin);
	for(i = 0; i < SDCARD_INIT_SCK_PULSES; i = i + 1)																		{ spi_io(0xff); }

	// Soft-Reset Card
	if(sdcard_issue_cmd(sdc, SDCARD_CMD_IDLE, 0) != SDCARD_FLAG_IDLE)														{ return 1; }

	// Init CHKV
	if(sdcard_init_chkv(sdc))																								{ return 1; }

	// Initialize Card (attempt to detect SDHC)
	i = 0;
	while((sdcard_issue_acmd(sdc, SDCARD_ACMD_INIT, SDCARD_ACMD_INIT_FLAG_HCS) != 0) && (i < SDCARD_INIT_TIMEOUT))			{ _delay_ms(SDCARD_DELAY_INIT); i = i + 1; }
	if(i >= SDCARD_INIT_TIMEOUT)																							{ return 1; }

	// Read OCR { Check SDHC }
	if((sdcard_read_ocr(sdc) & SDCARD_OCR_FLAG_CCS) != 0)																	{ sdc->sdcard_type = SDCARD_TYPE_SDHC; }

	// Set Block Size
	if(sdcard_issue_cmd(sdc, SDCARD_CMD_SBLK, SDCARD_BLOCK_SIZE))															{ return 1; }

	/* Read Capacity
	 */

	// Read CSD
	if(sdcard_read_csd(sdc, csd))																							{ return 1; }

	// Compute Capacity
	sdc->sdcard_capacity = (((((uint32_t)csd[7] & 0x3F) << 16) | ((uint16_t)csd[8] << 8) | csd[9]) + 1) * 1024;

	return 0;
}

// Init CHKV
uint8_t sdcard_init_chkv(struct sdcard *sdc)
{
	uint32_t r = 0;

	// Select Card
	dio_lo(sdc->cs_pin);
	_delay_us(SDCARD_DELAY_SELECT);

	// Issue CHKV Command
	sdcard_cmd(sdc, SDCARD_CMD_CHKV, SDCARD_CHKV_DEF_VAL);

	// Wait for Response
	if(sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT) != SDCARD_FLAG_IDLE)											{ dio_hi(sdc->cs_pin); return 1; }

	// Read Return Value
	spi_rx(&r, sizeof(uint32_t));

	// De-Select Card
	dio_hi(sdc->cs_pin);

	return !(bswap_l(r) == SDCARD_CHKV_DEF_VAL);
}

// Read Blocks
uint8_t sdcard_read_blocks(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count)
{
	uint8_t r;
	uint8_t i;
	uint8_t j;

	// Fix Block Address
	if(sdc->sdcard_type == SDCARD_TYPE_SD)																					{ block = block * SDCARD_BLOCK_SIZE; }

	// Select Card
	dio_lo(sdc->cs_pin);

	// Issue Read Multiple Blocks Command
	r = 0;
	if(sdcard_wait_until_equal(sdc, 0xff, SDCARD_IO_TIMEOUT) != 0xff)														{ goto fail; }
	sdcard_cmd(sdc, SDCARD_CMD_RMUL, block);
	r = sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT);
	if(r != 0)																												{ goto fail; }

	// Loop through Data Packets
	for(i = 0; i < count; i = i + 1)
	{
		// Wait for Data Packet Start (Read Token)
		if(sdcard_wait_until_diff(sdc, 0xff, SDCARD_IO_TIMEOUT) != SDCARD_DATA_TOKEN_READ)									{ goto fail; }

		// Read Data Block
		for(j = 0; j < (SDCARD_BLOCK_SIZE / SDCARD_IO_CHUNK_SIZE); j = j + 1)												{ spi_rx(&(buffer[(i * SDCARD_BLOCK_SIZE) + (j * SDCARD_IO_CHUNK_SIZE)]), SDCARD_IO_CHUNK_SIZE); }

		// Read CRC (2 Bytes)
		spi_rx(&j, 1);
		spi_rx(&j, 1);
	}

	// Terminate Transaction (Issue Stop Command - Discard One Byte)
	sdcard_cmd(sdc, SDCARD_CMD_STOP, 0);

	// Skip Byte
	spi_rx(&j, 1);

	// Wait while Card is Busy
	if(sdcard_wait_until_equal(sdc, 0xff, SDCARD_IO_TIMEOUT) != 0xff)														{ goto fail; }

	// De-Select Card
	dio_hi(sdc->cs_pin);

	// Done
	return 0;

	// On Error
	fail:

	// De-Select Card
	dio_hi(sdc->cs_pin);

	// Fail
	return 1;
}

// Write Blocks
uint8_t sdcard_write_blocks(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count)
{
	uint8_t r;
	uint8_t i;
	uint8_t j;

	// Fix Block Address
	if(sdc->sdcard_type == SDCARD_TYPE_SD)																					{ block = block * SDCARD_BLOCK_SIZE; }

	// Select Card
	dio_lo(sdc->cs_pin);

	// Issue Write Multiple Blocks Command
	if(sdcard_wait_until_equal(sdc, 0xff, SDCARD_IO_TIMEOUT) != 0xff)														{ goto fail; }
	sdcard_cmd(sdc, SDCARD_CMD_WMUL, block);
	if(sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT) != 0)															{ goto fail; }

	// Loop through Data Packets
	for(i = 0; i < count; i = i + 1)
	{
		// Send Data Token
		r = SDCARD_DATA_TOKEN_WRITE;
		spi_tx(&r, 1);

		// Write Data Block
		for(j = 0; j < (SDCARD_BLOCK_SIZE / SDCARD_IO_CHUNK_SIZE); j = j + 1)
		{
			spi_tx(&(buffer[(i * SDCARD_BLOCK_SIZE) + (j * SDCARD_IO_CHUNK_SIZE)]), SDCARD_IO_CHUNK_SIZE);
		}

		// Write CRC (2 Bytes)
		r = 0xff;
		spi_tx(&r, 1);
		spi_tx(&r, 1);

		// Read Data Response
		spi_rx(&r, 1);

		// Check Data Response
		if((r & SDCARD_DATA_RESP_MASK) != SDCARD_DATA_RESP_OK)																{ goto fail; }

		// Wait while Card is Busy
		if(sdcard_wait_until_equal(sdc, 0xff, SDCARD_IO_TIMEOUT) != 0xff)													{ goto fail; }
	}

	// Terminate Transaction (Issue Stop Token)
	spi_rx(&r, 1);
	spi_rx(&r, 1);
	r = SDCARD_DATA_TOKEN_STOP;
	spi_tx(&r, 1);
	spi_rx(&r, 1);

	// Wait while Card is Busy
	if(sdcard_wait_until_equal(sdc, 0xff, SDCARD_IO_TIMEOUT) != 0xff)														{ goto fail; }

	// De-Select Card
	dio_hi(sdc->cs_pin);

	// Verify
	_delay_ms(1);
	dio_lo(sdc->cs_pin);
	if(sdcard_wait_until_equal(sdc, 0xff, SDCARD_IO_TIMEOUT) != 0xff)														{ goto fail; }
	sdcard_cmd(sdc, SDCARD_CMD_STAT, 0);
	r = sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT);
	spi_rx(&r, 1);
	dio_hi(sdc->cs_pin);

	// Done
	return 0;

	// On Error
	fail:

	// De-Select Card
	dio_hi(sdc->cs_pin);

	// Fail
	return 1;
}

// Read Blocks with retry
uint8_t sdcard_read_with_retry(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count)
{
	uint8_t i;
	for(i = 0; i < SDCARD_MAX_RETRY; i = i + 1)
	{
		if(sdcard_read_blocks(sdc, block, buffer, count) == 0)																{ return 0; }
		else
		{
			// Re-init
			_delay_ms(SDCARD_RETRY_DELAY);
			sdcard_init_card(sdc);
			_delay_ms(SDCARD_RETRY_DELAY);
		}
	}

	return 1;
}

// Write Blocks with retry
uint8_t sdcard_write_with_retry(struct sdcard *sdc, uint32_t block, uint8_t *buffer, uint8_t count)
{
	uint8_t i;
	for(i = 0; i < SDCARD_MAX_RETRY; i = i + 1)
	{
		if(sdcard_write_blocks(sdc, block, buffer, count) == 0)																{ return 0; }
		else
		{
			// Re-init
			_delay_ms(SDCARD_RETRY_DELAY);
			sdcard_init_card(sdc);
			_delay_ms(SDCARD_RETRY_DELAY);
		}
	}

	return 1;
}

// Issue App Command (selects card)
uint8_t sdcard_issue_acmd(struct sdcard *sdc, uint8_t cmd, uint32_t arg)
{
	uint8_t r = 0;

	// Select Card
	dio_lo(sdc->cs_pin);

	// Issue App Command & Check Response
	sdcard_cmd(sdc, SDCARD_CMD_ACMD, 0);
	if(sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT) != SDCARD_FLAG_IDLE)											{ goto end; }

	// Issue Command & Fetch Response
	sdcard_cmd(sdc, cmd, arg);
	r = sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT);

	// End
	end:

	// De-Select Card
	dio_hi(sdc->cs_pin);

	return r;
}

// Issue Simple Command (selects card)
uint8_t sdcard_issue_cmd(struct sdcard *sdc, uint8_t cmd, uint32_t arg)
{
	uint8_t r = 0;

	// Select Card
	dio_lo(sdc->cs_pin);

	// Issue Command
	sdcard_cmd(sdc, cmd, arg);

	// Wait for Response
	r = sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT);

	// De-Select Card
	dio_hi(sdc->cs_pin);

	return r;
}

// Read OCR
uint32_t sdcard_read_ocr(struct sdcard *sdc)
{
	uint32_t r = 0;

	// Select Card
	dio_lo(sdc->cs_pin);

	// Issue Read OCR Command & Wait for Response
	sdcard_cmd(sdc, SDCARD_CMD_ROCR, 0);
	if(sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT) != 0)															{ goto end; }

	// Read OCR
	spi_rx(&r, sizeof(uint32_t));

	// End
	end:

	// De-Select Card
	dio_hi(sdc->cs_pin);

	return bswap_l(r);
}

// Read CSD
uint8_t sdcard_read_csd(struct sdcard *sdc, uint8_t *buffer)
{
	uint8_t r;

	// Select Card
	dio_lo(sdc->cs_pin);

	// Issue Read CSD Command & Wait for Response
	sdcard_cmd(sdc, SDCARD_CMD_RCSD, 0);
	if(sdcard_wait_until_diff(sdc, 0xff, SDCARD_CMD_TIMEOUT) != 0)															{ goto fail; }

	// Wait for Data Packet Start (Read Token)
	if(sdcard_wait_until_diff(sdc, 0xff, SDCARD_IO_TIMEOUT) != SDCARD_DATA_TOKEN_READ)										{ goto fail; }

	// Read CSD
	spi_rx(buffer, SDCARD_CSD_LEN);

	// Read CRC
	spi_rx(&r, 1);
	spi_rx(&r, 1);

	// De-Select Card
	dio_hi(sdc->cs_pin);

	// Done
	return 0;

	// Fail
	fail:

	// De-Select Card
	dio_hi(sdc->cs_pin);

	return 1;
}

// Send Command
void sdcard_cmd(struct sdcard *sdc, uint8_t cmd, uint32_t arg)
{
	uint8_t buf[6];

	// Wait until Ready
	sdcard_wait_until_equal(sdc, 0xff, SDCARD_CMD_TIMEOUT);

	// Write Command, Argument & Checksum
	buf[0] = cmd | SDCARD_CMD_PREFIX;
	buf[1] = (arg >> 24) & 0x000000ff;
	buf[2] = (arg >> 16) & 0x000000ff;
	buf[3] = (arg >> 8) & 0x000000ff;
	buf[4] = arg & 0x000000ff;
	if(cmd == SDCARD_CMD_IDLE)																								{ buf[5] = 0x95; }
	else if(cmd == SDCARD_CMD_CHKV)																							{ buf[5] = 0x87; }
	else																													{ buf[5] = 0xff; }

	// Send Command Frame
	spi_tx(buf, 6);
}

// Wait until equal
uint8_t sdcard_wait_until_equal(struct sdcard *sdc, uint8_t x, uint16_t timeout)
{
	uint16_t t = 0;
	uint8_t v = (x == 0) ? 1 : 0;
	while((v != x) && (t < timeout))																						{ spi_rx(&v, 1); t = t + 1; }
	return v;
}

// Wait until different
uint8_t sdcard_wait_until_diff(struct sdcard *sdc, uint8_t x, uint16_t timeout)
{
	uint16_t t = 0;
	uint8_t v = x;
	while((v == x) && (t < timeout))																						{ spi_rx(&v, 1); t = t + 1; }
	return v;
}
