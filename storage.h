/* Dooba SDK
 * SD Card Driver
 */

#ifndef	__SDCARD_STORAGE_H
#define	__SDCARD_STORAGE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <storage/storage.h>

// Internal Includes
#include "sdcard.h"
#include "blkcache.h"

// States
#define	SDCARD_STORAGE_OFF				0
#define	SDCARD_STORAGE_RDY				1

// SD Card Storage Structure
struct sdcard_storage
{
	// Storage
	struct storage st;

	// SD Card
	struct sdcard sdc;

	// Block Cache
	struct sdcard_blkcache blkcache;

	// State
	uint8_t state;
};

// Initialize SD Card as Storage Device
extern void sdcard_storage_init(struct sdcard_storage *sdcs, uint8_t cs_pin);

// Read Data
extern uint8_t sdcard_storage_read(struct storage *s, struct sdcard_storage *sdcs, uint64_t addr, void *data, uint16_t size);

// Write Data
extern uint8_t sdcard_storage_write(struct storage *s, struct sdcard_storage *sdcs, uint64_t addr, void *data, uint16_t size);

// Synchronize
extern uint8_t sdcard_storage_sync(struct storage *s, struct sdcard_storage *sdcs);

#endif
